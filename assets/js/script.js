const addInput = document.querySelector("#add-input");
const addButton = document.getElementById("add-button");
const taskContainer = document.querySelector(".task-container > ul");
const completedContainer = document.querySelector(".completed-container > ul");

let lists = [];

addInput.addEventListener("keypress", function(e) {
    if(e.key == "Enter") {
        if(this.value.trim() !== "") {
            lists.push({name: `${this.value.trim()}`, status: "pending"});
            this.value = "";
            displayPending();
            // console.log(list);
        }
    }
});

addButton.addEventListener("click", function () {
    if(addInput.value.trim() !== "") {
        lists.push({name: `${addInput.value.trim()}`, status: "pending"});
        addInput.value = "";
        displayPending();
        // console.log(lists);
    }
});

taskContainer.addEventListener("click", function (e) {
    if(e.target.classList.contains("task")) {
        lists.forEach(function (value, index) {
            if(value.name === e.target.textContent) {
                value.status = "done";
            }
        });
        displayPending();
        displayDone();
        // console.log(e.target.textContent);
        // console.log(lists.name.indexOf(e.target.textContent));
    }

    if(e.target.classList.contains("fa-window-close")) {
        lists.forEach(function (value, index) {
            if(value.name === e.target.parentNode.textContent) {
                lists.splice(index, 1);
            }
        });
        displayPending();
        displayDone();
        console.log(lists);
    }
});

completedContainer.addEventListener("click", function (e) {
    if(e.target.classList.contains("fa-window-close")) {
        lists.forEach(function (value, index) {
            if(value.name === e.target.parentNode.textContent) {
                lists.splice(index, 1);
            }
        });
        displayPending();
        displayDone();
        console.log(lists);
    }
});

function displayPending() {
    taskContainer.innerHTML = "";
    lists.forEach(function (element) {
        if(element.status === "pending") {
            taskContainer.innerHTML += `<li class="task">${element.name}<i class="fa fa-window-close fa-pull-right fa-2x"></i></li>`;
        }
    });
}

function displayDone() {
    completedContainer.innerHTML = "";
    lists.forEach(function (element) {
        if(element.status === "done") {
            completedContainer.innerHTML += `<li class="task"><strike>${element.name}</strike><i class="fa fa-window-close fa-pull-right fa-2x"></i></li>`;
        }
    });
}
